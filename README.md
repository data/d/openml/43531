# OpenML dataset: Gender-Classification-Dataset

https://www.openml.org/d/43531

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
While I was practicing machine learning, I wanted to create a simple dataset that is closely aligned to the real world scenario and gives better results to whet my appetite on this domain. If you are a beginner who wants to try solving classification problems in machine learning and if you prefer achieving better results, try using this dataset in your projects which will be a great place to start.
Content
This dataset contains 7 features and a label column. 
longhair - This column contains 0's and 1's where 1 is "long hair" and 0 is "not long hair".
foreheadwidthcm - This column is in CM's. This is the width of the forehead. 
foreheadheightcm - This is the height of the forehead and it's in Cm's.
nosewide - This column contains 0's and 1's where 1 is "wide nose" and 0 is "not wide nose". 
noselong - This column contains 0's and 1's where 1 is "Long nose" and 0 is "not long nose".
lipsthin - This column contains 0's and 1's where 1 represents the "thin lips" while 0 is "Not thin lips".
distancenosetoliplong - This column contains 0's and 1's where 1 represents the "long distance between nose and lips" while 0 is "short distance between nose and lips".
gender - This is either "Male" or "Female".
Acknowledgements
Nothing to acknowledge as this is just a made up data.
Inspiration
It's painful to see bad results at the beginning. Don't begin with complicated datasets if you are a beginner. I'm sure that this dataset will encourage you to proceed further in the domain. Good luck.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43531) of an [OpenML dataset](https://www.openml.org/d/43531). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43531/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43531/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43531/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

